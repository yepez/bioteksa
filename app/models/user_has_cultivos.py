from conf.database import db
from sqlalchemy.sql import func


class UsersHasCultivos(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    cultivos_id = db.Column(db.Integer, db.ForeignKey("cultivos.id"), nullable=True)
    users_id = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    deleted_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
