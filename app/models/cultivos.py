from conf.database import db
from sqlalchemy.sql import func


class Cultivos(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    nombre = db.Column(db.String(100), nullable=False)
    polyline = db.Column(db.Text(), nullable=False)
    fecha_inicio = db.Column(db.DateTime, nullable=False, server_default=func.now())
    fecha_final = db.Column(db.DateTime, nullable=False, server_default=func.now())
    clave_cultivo = db.Column(db.String(10), nullable=False)
    predios_id = db.Column(db.Integer, nullable=False)
    tipos_cultivo_id = db.Column(db.Integer, nullable=False)
    ambiente_cultivo_id = db.Column(db.Integer, nullable=False)
    ciclo_cultivo_id = db.Column(db.Integer, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    deleted_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    creador_id = db.Column(
        db.Integer, db.ForeignKey("bio_dispositivos.propietario_id"), nullable=False
    )
    zonas_horarias_id = db.Column(db.Integer, nullable=False)
    hectareas = db.Column(db.Boolean, nullable=False)
    bio_dispositivos = db.relationship("BioDispositivos", back_populates="cultivos")
