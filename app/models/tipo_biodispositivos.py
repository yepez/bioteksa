from conf.database import db
from sqlalchemy.sql import func


class TipoBiodispositivos(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    tipo = db.Column(db.String(250), nullable=False)
    descripcion = db.Column(db.Text(), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    deleted_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    modulos = db.Column(db.Integer, nullable=False)
