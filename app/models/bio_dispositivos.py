from conf.database import db
from sqlalchemy.sql import func


class BioDispositivos(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    nombre = db.Column(db.String(250), nullable=False)
    identificador = db.Column(db.String(20), nullable=False)
    tipo_biodispositivos_id = db.Column(db.Integer, nullable=False)
    bio_dispositivos_id = db.Column(db.Integer, nullable=False)
    dispositivos_sms_id = db.Column(db.Integer, nullable=False)
    notas = db.Column(db.Text(), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    deleted_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    position = db.Column(db.Text(), nullable=False)
    clave = db.Column(db.String(10), nullable=False)
    propietario_id = db.Column(db.Integer, nullable=False)
    activo = db.Column(db.Integer, nullable=False)
    recarga_saldo = db.Column(db.Date, nullable=False, server_default=func.now())
    cultivos = db.relationship("Cultivos", back_populates="bio_dispositivos")
