from conf.database import db
from sqlalchemy.sql import func


class UsersHasBioDispositivos(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    users_id = db.Column(db.Integer, nullable=False)
    bio_dispositivos_id = db.Column(
        db.Integer, db.ForeignKey("bio_dispositivos.id"), nullable=True
    )
    created_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    deleted_at = db.Column(db.DateTime, nullable=False, server_default=func.now())
    write = db.Column(db.Integer, nullable=False)
