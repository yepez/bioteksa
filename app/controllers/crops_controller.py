from app.models.bio_dispositivos import BioDispositivos as BioDispositivosModel
from app.models.cultivos import Cultivos as CultivosModel
from app.models.sensores_has_bio_dispositivos import (
    SensoresHasBioDispositivos as SensoresHasBioDispositivosModel,
)
from app.models.tipo_biodispositivos import (
    TipoBiodispositivos as TipoBiodispositivosModel,
)
from app.models.user_has_cultivos import UsersHasCultivos as UsersHasCultivosModel
from conf.database import db
from flask import jsonify, request
from flask_restx import Resource

from ..dtos.crops import CropsDTO
from ..serializers.bio_dispositivos import BioDispositivosSchema
from ..serializers.cultivos import CultivosSchema
from ..serializers.sensores_has_biodispositivos import SensoresHasBioDispositivosSchema
from ..serializers.tipo_biodispositivos import TipoBioDispositivosSchema

api = CropsDTO.api


def message_statuscode(message, code=200):
    return message, code


def verify_body(requiered_parameter=""):

    if requiered_parameter != "":
        if requiered_parameter == "":
            return f"Missing {requiered_parameter} in body request", 400

    return "correct body", 200


@api.route("/<int:user_id>")
@api.param("user_id", "The user identifier")
@api.response(404, "User not found.")
class getCrops(Resource):
    @api.doc(
        responses={
            204: "Refund information received and sent",
            400: "Wrong Request",
            403: "Forbidden",
            404: "User not found.",
            412: "Precondition Failed",
        }
    )
    @api.response(200, "Success", CropsDTO.cultivo_base_response)
    def get(self, user_id):
        """Crops"""

        request_message, status_code = verify_body(
            requiered_parameter=user_id,
        )

        if status_code != 200:
            return request_message, status_code

        cultivos = (
            CultivosModel.query.join(
                UsersHasCultivosModel,
                CultivosModel.id == UsersHasCultivosModel.cultivos_id,
            )
            .filter(UsersHasCultivosModel.users_id == user_id)
            .all()
        )

        cultivo_schema = CultivosSchema()
        cultivosFound = [cultivo_schema.dump(item) for item in cultivos]

        if len(cultivosFound) > 0:

            bio_dispositivo_schema = BioDispositivosSchema()
            tipos_bio_dispositivo_schema = TipoBioDispositivosSchema()
            sensores_bio_dispositivo_schema = SensoresHasBioDispositivosSchema()

            dispositivosFound = BioDispositivosModel.query.filter(
                BioDispositivosModel.propietario_id == user_id,
                BioDispositivosModel.activo == 0,
            ).all()

            bio_dispositivos = [
                bio_dispositivo_schema.dump(dispositivo)
                for dispositivo in dispositivosFound
            ]

            response = []

            for cultivo in cultivosFound:
                cultivo["devices"] = bio_dispositivos

                for devices in cultivo["devices"]:
                    devices["pivot"] = {
                        "cultivos_id": cultivo["id"],
                        "bio_dispositivos_id": devices["id"],
                    }

                    sensoresFound = [
                        SensoresHasBioDispositivosModel.query.filter(
                            SensoresHasBioDispositivosModel.bio_dispositivos_id
                            == devices["id"]
                        ).all()
                    ]

                    sensores = [
                        sensores_bio_dispositivo_schema.dump(sensor[0])
                        for sensor in sensoresFound
                    ]

                    last_log = [
                        {
                            "value_datetime": "2017-10-11 04:17:43",
                            "pivot": {
                                "bio_dispositivos_id": devices["id"],
                                "sensores_id": sensor["sensores_id"],
                            },
                        }
                        for sensor in sensores
                    ]

                    devices["last_log"] = last_log

                    tiposFound = TipoBiodispositivosModel.query.filter(
                        TipoBiodispositivosModel.id
                        == devices["tipo_biodispositivos_id"]
                    ).all()

                    device_type = [
                        tipos_bio_dispositivo_schema.dump(dispositivo)
                        for dispositivo in tiposFound
                    ]

                    for tipo in device_type:
                        devices["device_type"] = {
                            "id": tipo["id"],
                            "nombre": tipo["nombre"],
                            "modulos": tipo["modulos"],
                        }

            response.append(cultivosFound)

            return jsonify({"data": response})
        return message_statuscode("User not found.", 404)
