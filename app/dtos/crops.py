from json import dumps as jdumps

from flask_restx import Namespace, fields


class CropsDTO:
    api = Namespace(
        " Crops ",
        description=""" En este método, obtiene el listado de sus cultivos y dentro de cada uno de sus cultivos se deberán listar los dispositivos que están en uso en ese cultivo. """,
    )

    crops_request = api.model(
        "crops_request",
        {
            "id_usuario": fields.Integer(required=True),
        },
    )

    cultivo = api.model(
        "cultivo",
        {
            "nombre": fields.String(required=True, example="Cuadrante"),
            "ciclo_cultivo_id": fields.Integer(required=True, example=1),
            "ambiente_cultivo_id": fields.Integer(required=True, example=2),
            "fecha_inicio": fields.String(
                required=True, description="Fecha Inicio", example="2019-01-01"
            ),
            "fecha_final": fields.String(
                required=True, description="Fecha Final", example="2019-02-20"
            ),
            "clave_cultivo": fields.String(
                required=True, description="Clave Cultivo", example="a0qw5YYg"
            ),
            "creador_id": fields.Integer(required=True, example=1),
            "id": fields.Integer(required=True, example=27),
            "predios_id": fields.Integer(required=True, example=7),
            "tipos_cultivo_id": fields.Integer(required=True, example=1),
        },
    )

    pivot = api.model(
        "pivot",
        {
            "cultivos_id": fields.Integer(required=True, example=27),
            "bio_dispositivos_id": fields.Integer(required=True, example=48),
        },
    )

    last_log_pivot = api.model(
        "last_log_pivot",
        {
            "bio_dispositivos_id": fields.Integer(required=True, example=48),
            "sensores_id": fields.Integer(required=True, example=108),
        },
    )

    last_log = api.model(
        "last_log",
        {
            "value_datetime": fields.String(
                required=True, example="2017-10-11 04:17:43"
            ),
            "pivot": fields.Nested(last_log_pivot),
        },
    )

    device_type = api.model(
        "device_type",
        {
            "id": fields.Integer(required=True, example=4),
            "nombre": fields.String(required=True, example="BOTLOG R3"),
            "modulos": fields.Integer(required=True, example=0),
        },
    )

    devices = api.model(
        "devices",
        {
            "nombre": fields.String(required=True, example="LOTE 18"),
            "clave": fields.String(required=True, example="ZdHOeo5s"),
            "id": fields.Integer(required=True, example=48),
            "tipo_biodispositivos_id": fields.Integer(required=True, example=4),
            "pivot": fields.Nested(pivot),
            "last_log": fields.List(fields.Nested(last_log)),
            "device_type": fields.Nested(device_type),
        },
    )

    devices_base = api.inherit(
        "devices_base",
        cultivo,
        {"devices": fields.List(fields.Nested(devices))},
    )

    cultivo_base = api.model(
        "cultivo_base",
        {"data": fields.List(fields.Nested(required=True, model=devices_base))},
    )

    cultivo_base_response = api.model("cultivo_base_response", cultivo_base)
