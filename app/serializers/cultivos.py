from marshmallow import Schema, fields, post_load

from ..models.cultivos import Cultivos
from ..serializers.bio_dispositivos import BioDispositivosSchema


class CultivosSchema(Schema):
    nombre = fields.Str(required=True)
    ciclo_cultivo_id = fields.Int(required=True)
    ambiente_cultivo_id = fields.Int(required=True)
    fecha_inicio = fields.Str(required=True)
    fecha_final = fields.Str(required=True)
    clave_cultivo = fields.Str(required=True)
    creador_id = fields.Int(required=True)
    id = fields.Int(required=True)
    predios_id = fields.Int(required=True)
    tipos_cultivo_id = fields.Int(required=True)
    devices = fields.List(fields.Nested(BioDispositivosSchema), required=True)

    @post_load
    def make_cultivos(self, data, **kwargs):
        return Cultivos(**data)
