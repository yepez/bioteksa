from marshmallow import Schema, fields, post_load

from ..models.tipo_biodispositivos import TipoBiodispositivos


class TipoBioDispositivosSchema(Schema):
    id = fields.Int(required=True)
    tipo = fields.Str(required=True, data_key="nombre")
    modulos = fields.Int(required=True)

    @post_load
    def make_tipo_biodispositivos(self, data, **kwargs):
        return TipoBiodispositivos(**data)
