from marshmallow import Schema, fields, post_load

from ..models.sensores_has_bio_dispositivos import SensoresHasBioDispositivos


class SensoresHasBioDispositivosSchema(Schema):
    id = fields.Int(required=True)
    sensores_id = fields.Int(required=True)
    bio_dispositivos_id = fields.Int(required=False)

    @post_load
    def make_sensores_has_biodispositivos(self, data, **kwargs):
        return SensoresHasBioDispositivos(**data)
