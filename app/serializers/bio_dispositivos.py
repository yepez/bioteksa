from marshmallow import Schema, fields, post_load

from ..models.bio_dispositivos import BioDispositivos


class BioDispositivosSchema(Schema):
    nombre = fields.Str(required=True)
    clave = fields.Str(required=True)
    id = fields.Int(required=True)
    tipo_biodispositivos_id = fields.Int(required=True)

    @post_load
    def make_bio_dispositivos(self, data, **kwargs):
        return BioDispositivos(**data)
