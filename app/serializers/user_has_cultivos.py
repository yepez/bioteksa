from marshmallow import Schema, fields, post_load

from ..models.user_has_cultivos import UsersHasCultivos


class UserHasCultivosSchema(Schema):
    id = fields.Str(required=True)
    cultivos_id = fields.Str(required=True)
    users_id = fields.Str(required=False)

    @post_load
    def make_user_has_cultivos(self, data, **kwargs):
        return UsersHasCultivos(**data)
