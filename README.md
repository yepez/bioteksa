# API Rest Bioteksa

Using Flask to build a Restful API Server with Swagger document.

Integration with Flask-restx, Flask-SQLalchemy extensions.

## Software

- [Python 3.8.x](https://www.python.org/downloads/release/python-3811/)
- [Flask](https://flask.palletsprojects.com/en/2.0.x/)

## Installation

Install with pip:

```
$ pip install -r requirements.txt
```

## Flask Application Structure

```
.
|──────app/
| |────**init**.py
| |────conf/
| | |────__init__.py
| | |────database.py
| |────controllers/
| | |────crops_controller.py
| |────dptos/
| | |────crops.py
| |────models/
| | |────bio_dispositivos.py
| | |────cultivos.py
| | |────sensores_has_bio_dispositivos.py
| | |────tipo_biodispositivos.py
| | |────user_has_biodispositivos.py
| | |────user_has_cultivos.py
| |────serializers/
| | |────bio_dispositivos.py
| | |────cultivos.py
| | |────sensores_has_biodispositivos.py
| | |────tipo_biodispositivos.py
| | |────user_has_cultivos.py
|──────main.py
```

### Variables de entorno

Para que el bioteksa funcione debes crear las siguientes variables de entorno:

#### Linux/Mac

    export FLASK_APP="main"
    export FLASK_ENV="development"

#### Windows

    set "FLASK_APP=main"
    set "FLASK_ENV=development"
