from flask import Flask
from flask_restx import Api

from app.controllers.crops_controller import api as crops_ns
from conf import MySQL, ServerData
from conf.database import db

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = (
    "mysql://"
    + MySQL.user
    + ":"
    + MySQL.password
    + "@"
    + MySQL.host
    + ":"
    + MySQL.port
    + "/"
    + MySQL.database
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = "False"

db.init_app(app)

api = Api(
    app=app,
    title="Bioteksa",
    description=""" Se trata de una API que obtiene el listado de sus cultivos por usuario. """,
    openapi="2.0.0",
    version="0.0.1",
    termsOfService="",
)


api.add_namespace(crops_ns, path="/crops")

# Local
app.run(port=ServerData.port, debug=ServerData.debug_status, host=ServerData.host)
